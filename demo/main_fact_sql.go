/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0

main_fact_sql
*/

package main

import (
	"chainmaker.org/contract-sdk-tinygo/sdk/convert"
)

//安装合约时会执行此方法，必须
//export init_contract
func initContract() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()
	// 创建表 sql语句
	sqlCreateStudent := `create table student_gasm (
					id varchar(128) primary key,
					teacher_id varchar(128),
					name varchar(64) DEFAULT '',
					age int DEFAULT 0,
					score int DEFAULT 0,
					id_card_no varchar(19) DEFAULT '',
					attend_school_time date
					)
				`
	// 打印日志
	ctx.Log("init contract sql: " + sqlCreateStudent)
	// 执行DDL语句
	_, resultCode := ctx.ExecuteDdl(sqlCreateStudent)
	// 校验执行结果
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlCreateStudent=" + sqlCreateStudent
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("create table student_gasm success.")
	}
	// 返回信息
	ctx.SuccessResult("create table student、teacher_gasm success")
	// 打印日志便于调试
	ctx.Log("initContract success.")
}

// 升级合约时会执行此方法，必须
//export upgrade
func upgrade() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()
	ctx.Log("upgrade [start]")

	// 修改表结构 sql语句
	sqlAddColumn := "ALTER TABLE student_gasm ADD address varchar(255) NULL"
	// 记录执行语句
	ctx.Log("upgrade contract sql: " + sqlAddColumn)
	// 执行DDL语句
	_, resultCode := ctx.ExecuteDdl(sqlAddColumn)

	// 校验执行结果
	if resultCode != SUCCESS {
		msg := "upgrade error."
		ctx.Log(msg)
		// 返回信息
		ctx.ErrorResult(msg)
	} else {
		ctx.Log("upgrade success.")
		// 返回信息
		ctx.SuccessResult("upgrade success.")
	}
}

// 插入一条数据
//export sql_insert
func sqlInsert() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()

	// 获取参数
	id, _ := ctx.ArgString("id")
	age, _ := ctx.ArgString("age")
	name, _ := ctx.ArgString("name")
	idCardNo, _ := ctx.ArgString("id_card_no")

	// 校验参数
	if len(id) == 0 || len(age) == 0 {
		ctx.Log("param id/age is required")
		ctx.ErrorResult("param id/age is required")
		return
	}

	// 插入数据 sql语句
	sqlInsert := "insert into student_gasm(id, name, age, id_card_no) VALUES ('" + id + "', '" + name + "', '" + age + "', '" + idCardNo + "')"
	ctx.Log("insert data sql:" + sqlInsert)

	// 执行DML语句
	rowCount, resultCode := ctx.ExecuteUpdate(sqlInsert)

	// 校验执行结果
	if resultCode != SUCCESS {
		ctx.Log("sql_insert error")
		ctx.ErrorResult("sql_insert error")
		return
	} else {
		msg := "sql_insert update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	// 返回信息
	ctx.SuccessResult("ok")
}

// 查询一条数据
//export sql_query_by_id
func sqlQueryById() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()

	// 获取参数
	id, _ := ctx.ArgString("id")
	if len(id) == 0 {
		ctx.Log("param id is required")
		ctx.ErrorResult("param id is required")
		return
	}

	// 查询数据 sql语句
	sqlQuery := "select id, name, age, id_card_no from student_gasm where id='" + id + "'"
	ctx.Log("query data sql:" + sqlQuery)

	// 执行DQL语句
	ec, resultCode := ctx.ExecuteQueryOne(sqlQuery)

	// 校验执行结果
	if resultCode != SUCCESS {
		ctx.Log("ExecuteQueryOne error")
		ctx.ErrorResult("ExecuteQueryOne error")
		return
	}
	jsonStr := ec.ToJson()
	ctx.Log("sql_query_by_id ok result:" + jsonStr)
	// 返回信息
	ctx.SuccessResult(jsonStr)
}

// 范围查询
//export sql_query_range_of_age
func sqlQueryRangeOfAge() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()

	// 获取参数
	maxAge, _ := ctx.ArgString("max_age")
	minAge, _ := ctx.ArgString("min_age")
	// 校验参数
	if len(maxAge) == 0 || len(minAge) == 0 {
		ctx.Log("param max_age/min_age is required")
		ctx.ErrorResult("param max_age/min_age is required")
		return
	}
	if minAge >= maxAge {
		ctx.Log("param max_age/min_age is invalid")
		ctx.ErrorResult("param max_age/min_age is invalid")
		return
	}

	// 查询数据 sql语句
	sqlQuery := "select id, name, age, id_card_no from student_gasm where age>" + minAge + " and age<" + maxAge
	ctx.Log("query data sql:" + sqlQuery)

	// 执行DQL语句
	resultSet, resultCode := ctx.ExecuteQuery(sqlQuery)
	if resultCode != SUCCESS {
		ctx.Log("ExecuteQuery error")
		ctx.ErrorResult("ExecuteQuery error")
		return
	}

	var count int
	// 判断结果集是否还有数据
	for resultSet.HasNext() {
		// 获取下一行结果
		ec, resultCode := resultSet.NextRow()
		if resultCode != SUCCESS {
			ctx.Log("NextRow error")
			ctx.ErrorResult("NextRow error")
			return
		}
		jsonStr := ec.ToJson()
		ctx.Log("NextRow: " + jsonStr)
		count++
	}
	// 关闭迭代器【必须】
	resultSet.Close()

	// 返回信息
	ctx.SuccessResult(convert.Int32ToString(int32(count)))
}

// 更新数据
//export sql_update
func sqlUpdate() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()

	// 获取参数
	name, _ := ctx.ArgString("name")
	id, _ := ctx.ArgString("id")

	// 更新数据 sql语句
	sqlUpdate := "update student_gasm set name='" + name + "' where id='" + id + "'"
	ctx.Log("update data sql:" + sqlUpdate)

	// 执行DML语句
	rowCount, resultCode := ctx.ExecuteUpdate(sqlUpdate)

	// 校验结果
	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("ExecuteUpdateSingle error")
		return
	} else {
		msg := "ExecuteUpdateSingle update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	// 返回信息
	ctx.SuccessResult("ok")
}

// 删除数据
//export sql_delete
func sqlDelete() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()

	id, _ := ctx.ArgString("id")

	// 删除数据 sql语句
	sqlDelete := "delete from student_gasm where id='" + id + "'"
	ctx.Log("delete data sql:" + sqlDelete)

	// 执行DML语句
	rowCount, resultCode := ctx.ExecuteUpdate(sqlDelete)

	// 校验结果
	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("ExecuteUpdateSingle error")
		return
	} else {
		msg := "ExecuteUpdateSingle update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	// 返回影响行数
	ctx.SuccessResult(convert.Int32ToString(rowCount))
}

// 执行不存在表的操作
//export sql_option_not_exist_table
func sqlOptionNotExistTable() {
	// 获取SDK SQL对象
	ctx := NewSqlSimContext()

	option, _ := ctx.ArgString("option")

	var sql string
	switch option {
	case "insert":
		sql = "insert into notExistTable(id, name) values('id', 'name')"
	case "update":
		sql = "update notExistTable set name='abc'"
	case "delete":
		sql = "delete from notExistTable where id='id'"
	case "query":
		sql = "select * from notExistTable"
		ctx.ExecuteQuery(sql)
		ctx.Log("sql:" + sql)
		return
	default:
		ctx.Log("not found available option")
		ctx.ErrorResult("not found available option")
		return

	}
	ctx.Log("sql:" + sql)

	// 执行DML语句
	rowCount, resultCode := ctx.ExecuteUpdate(sql)
	// 校验结果
	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("ExecuteUpdateSingle error")
		return
	} else {
		msg := "ExecuteUpdateSingle update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	// 返回影响行数
	ctx.SuccessResult(convert.Int32ToString(rowCount))
}

func main() {

}
