/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0

main_functional_verify.go
*/
package main

import (
	"chainmaker.org/contract-sdk-tinygo/sdk/convert"
)

// 安装合约时会执行此方法，必须
//export init_contract
func initContract() {
	LogMessage("===================================init_contract===================================")
	LogMessage("===================================init_contract===================================")
	LogMessage("===================================init_contract===================================")
	ctx := NewSimContext()
	ctx.Log("=====NewSimContext")
	LogMessage("=====NewSimContext")
	ctx.PutState("init_key", "init_field", "init_value")
	LogMessage("=====PutState")
	val, code := ctx.GetState("init_key", "init_field")
	LogMessage("=====GetState")
	if code != SUCCESS {
		LogMessage("=====code != SUCCESS")
		ctx.Log("GetState error")
		ctx.ErrorResult("GetState error")
		LogMessage("=====ErrorResult1")
		return
	}
	if val != "init_value" {
		LogMessage("=====val != \"init_value\"")
		ctx.Log("GetState val not init_value val=" + val)
		ctx.ErrorResult("GetState val not init_value val=" + val)
		LogMessage("=====ErrorResult2")
		return
	}
	ctx.SuccessResult("install contract ok")
	LogMessage("=====SuccessResult install contract ok")
}

// 升级合约时会执行此方法，必须
//export upgrade
func upgrade() {
	LogMessage("===================================upgrade_contract===================================")
	LogMessage("===================================upgrade_contract===================================")
	LogMessage("===================================upgrade_contract===================================")
	ctx := NewSimContext()
	ctx.Log("=====NewSimContext")
	LogMessage("=====NewSimContext")
	ctx.PutState("upgrade_key", "upgrade_field", "upgrade_value")
	LogMessage("=====PutState")
	val, code := ctx.GetState("upgrade_key", "upgrade_field")
	LogMessage("=====GetState")
	if code != SUCCESS {
		LogMessage("=====code != SUCCESS")
		ctx.Log("GetState error")
		ctx.ErrorResult("GetState error")
		LogMessage("=====ErrorResult1")
		return
	}
	if val != "upgrade_value" {
		LogMessage("=====val != \"upgrade_value\"")
		ctx.Log("GetState val not upgrade_value val=" + val)
		ctx.ErrorResult("GetState val not upgrade_value val=" + val)
		LogMessage("=====ErrorResult2")
		return
	}
	ctx.SuccessResult("install contract ok")
	LogMessage("=====SuccessResult install contract ok")
}

//export save
func save() {
	LogMessage("save [start] ")
	// 获取参数
	time, _ := ArgString("time")
	fileHash, _ := ArgString("file_hash")
	fileName, _ := ArgString("file_name")

	// 组装
	ec := NewEasyCodec()
	ec.AddString("file_hash", fileHash)
	ec.AddString("file_name", fileName)
	ec.AddString("time", time)

	// 存储数据
	PutState("fact", fileHash, ec.ToJson())
	// 返回结果
	SuccessResult("ok")
	LogMessage("save [end] ")
}

//export find_by_file_hash
func findByFileHash() {
	LogMessage("find_by_file_hash [start] ")
	// 获取参数
	fileHash, _ := ArgString("file_hash")
	// 查询
	if result, resultCode := GetStateByte("fact", fileHash); resultCode != SUCCESS {
		// 返回结果
		ErrorResult("failed to call get_state, only 64 letters and numbers are allowed. got key:" + "fact" + ", field:" + fileHash)
	} else {
		// 记录日志
		LogMessage("get val:" + string(result))
		// 返回结果
		SuccessResultByte(result)
	}
	LogMessage("find_by_file_hash [end] ")
}

//export sum
func sum() {
	LogMessage("sum [start] ")
	arg1, _ := ArgString("arg1")
	arg2, _ := ArgString("arg2")
	LogMessage("get arg1=" + arg1 + " arg2=" + arg2)
	num1, _ := convert.StringToInt32(arg1)
	num2, _ := convert.StringToInt32(arg2)
	SuccessResult(convert.Int32ToString(num1 + num2))
	LogMessage("sum [end] ")
}

//export increase
func increase() {
	var heartbeatInt int32 = 0
	if heartbeatString, resultCode := GetState("Counter", "heartbeat"); resultCode != SUCCESS {
		heartbeatInt = 1
		LogMessage("call increase, put state from empty")
		PutState("Counter", "heartbeat", convert.Int32ToString(heartbeatInt))
	} else {
		heartbeatInt, _ = convert.StringToInt32(heartbeatString)
		heartbeatInt++
		LogMessage("call increase, put state from exist")
		PutState("Counter", "heartbeat", convert.Int32ToString(heartbeatInt))
	}
	SuccessResult(convert.Int32ToString(heartbeatInt))
}

//export query
func query() {
	LogMessage("call query")
	if heartbeatString, resultCode := GetState("Counter", "heartbeat"); resultCode != SUCCESS {
		ErrorResult("failed to query state")
	} else {
		LogMessage("call query, heartbeat:" + heartbeatString)
		SuccessResult(heartbeatString)
	}
}

//export functional_verify
func functionVerify() {
	LogMessage("===================================functionTest start===================================")
	// 取出数据
	LogMessage("===================================GetStateByte test===================================")
	if result, resultCode := GetStateByte("fact", "fileHash"); resultCode != SUCCESS {
		LogMessage("【error】get state")
	} else {
		LogMessage("get state success." + string(result))
		if len(result) == 0 {
			LogMessage("【success】get state")
		}
	}

	// 存储数据
	LogMessage("===================================PutState test===================================")
	code := PutState("fact", "fileHash", "this is string.")
	if code != SUCCESS {
		LogMessage("【error】put state")
	} else {
		LogMessage("【success】put state")
	}

	// 取出数据
	LogMessage("===================================GetStateByte test===================================")
	if result, resultCode := GetStateByte("fact", "fileHash"); resultCode != SUCCESS {
		LogMessage("【error】get state")
	} else {
		LogMessage("get state success." + string(result))
		if string(result) == "this is string." {
			LogMessage("【success】get state")
		} else {
			LogMessage("【error】get state")
		}
	}

	// 删除数据
	LogMessage("===================================DeleteState test===================================")
	code = DeleteState("fact", "fileHash")
	if code != SUCCESS {
		LogMessage("【error】del state")
	} else {
		LogMessage("【success】del state")
	}

	// 取出数据
	LogMessage("===================================GetStateByte test===================================")
	if result, resultCode := GetStateByte("fact", "fileHash"); resultCode != SUCCESS {
		LogMessage("【error】get state")
	} else {
		LogMessage("get state success." + string(result))
		if len(result) == 0 {
			LogMessage("【success】get state")
		}
	}

	// 存储数据
	LogMessage("===================================PutStateFromKey test===================================")
	code = PutStateFromKey("fact", "this is string.")
	if code != SUCCESS {
		LogMessage("【error】put state from key")
	} else {
		LogMessage("【success】put state from key")
	}

	// 取出数据
	LogMessage("===================================GetStateFromKey test===================================")
	if result, resultCode := GetStateFromKey("fact"); resultCode != SUCCESS {
		LogMessage("【error】get state")
	} else {
		LogMessage("get state success." + string(result))
		if string(result) == "this is string." {
			LogMessage("【success】get state")
		} else {
			LogMessage("【error】get state")
		}
	}

	// 删除数据
	LogMessage("===================================DeleteStateFromKey test===================================")
	code = DeleteStateFromKey("fact")
	if code != SUCCESS {
		LogMessage("【error】del state")
	} else {
		LogMessage("【success】del state")
	}
	// 取出数据
	LogMessage("===================================GetStateFromKey test===================================")
	if result, resultCode := GetStateFromKey("fact"); resultCode != SUCCESS {
		LogMessage("【error】get state")
	} else {
		LogMessage("get state success." + string(result))
		if len(result) == 0 {
			LogMessage("【success】get state")
		}
	}
	// marshal
	LogMessage("===================================EasyCodecItem test===================================")
	strVal := "chainmaker长安链!@#$%^&*()_+-={}|:?><"
	ec := NewEasyCodec()
	ec.AddBytes("keyBytes", []byte(strVal))
	ec.AddInt32("keyInt32", 123456789)
	ec.AddString("keyStr", strVal)
	bytes := ec.Marshal()
	// unmarshal
	ec = &EasyCodec{EasyUnmarshal(bytes)}

	valBytes, _ := ec.GetBytes("keyBytes")
	valInt32, _ := ec.GetInt32("keyInt32")
	valStr, _ := ec.GetString("keyStr")
	if string(valBytes) != strVal {
		LogMessage(" valBytes != " + strVal)
		panic("marshal unmarshal fail. valBytes !=  " + strVal)
	}
	if 123456789 != valInt32 {
		LogMessage(" valInt32 !=  1234567890")
		panic("marshal unmarshal fail. 1234567890 != valInt32 ")
	}
	if valStr != strVal {
		LogMessage(" valStr !=  " + strVal)
		panic("marshal unmarshal fail. valStr !=  " + strVal)
	}
	LogMessage("【success】 valBytes=" + string(valBytes))
	LogMessage("【success】 valStr=" + valStr)
	LogMessage("【success】 valInt32=" + convert.Int32ToString(valInt32))

	jsonStr := ec.ToJson()
	LogMessage("【success】 jsonStr = " + jsonStr)

	mapVal := make(map[string][]byte)
	byteVal := []byte(strVal)
	mapVal["keyStrBytes"] = byteVal
	mapVal["keyStrInt32"] = byteVal
	mapVal["keyStr"] = byteVal
	mapVal["keyStr2"] = byteVal
	mapVal["keyStr1"] = byteVal
	ec = NewEasyCodecWithMap(mapVal)
	jsonStr = ec.ToJson()
	LogMessage("【success】map easycodec jsonStr = " + jsonStr)
	// map 无序测试
	PutState("key", "val", jsonStr)

	// 跨合约调用
	LogMessage("===================================CallContract save test===================================")
	param := make(map[string][]byte)
	param["file_hash"] = []byte("staticVal2")
	param["file_name"] = []byte("staticVal3")
	param["time"] = []byte("12")
	contractName, _ := ArgString("contract_name")
	if len(contractName) == 0 {
		contractName = "contract01"
	}
	result, code := CallContract(contractName, "save", param)
	if code != SUCCESS {
		LogMessage("【error】call contract")
	} else {
		LogMessage("【success】call contract " + contractName + " save success.result=" + string(result))
	}

	// 跨合约调用
	LogMessage("===================================CallContract find_by_file_hash test===================================")
	if result, code := CallContract(contractName, "find_by_file_hash", param); code != SUCCESS {
		LogMessage("【error】call contract")
	} else {
		LogMessage("【success】call contract " + contractName + " find_by_file_hash success.result=" + string(result))
	}

	// 成功返回
	LogMessage("===================================SuccessResultByte test===================================")
	SuccessResultByte([]byte("SuccessResult test"))
	LogMessage("===================================SuccessResult test===================================")
	SuccessResult("SuccessResult test")
	// 错误返回
	LogMessage("===================================ErrorResult test===================================")
	//ErrorResult("ErrorResult test")
	LogMessage("===================================functionTest done===================================")

	ctx := NewSimContext()
	ctx.PutStateFromKey("key2", "2")
	data, _ := ctx.GetStateFromKey("key2")
	if string(data) != "2" {
		LogMessage("【ERROR GetStateFromKey】 val != 2, val = " + string(data))
		panic("【ERROR GetStateFromKey】 val != 2, val = " + string(data))
	}
	ctx.DeleteStateFromKey("key2")
	data, _ = ctx.GetStateFromKey("key2")
	if len(data) > 0 {
		LogMessage("【ERROR DeleteStateFromKey】 val != nil, val = " + string(data))
		panic("【ERROR DeleteStateFromKey】 val != nil, val = " + string(data))
	}
	ctx.SuccessResult("ok")
}

//export test_kv_iterator
func testKvIterator() {
	ctx := NewSimContext()
	key, _ := ctx.ArgString("key")
	limit, _ := ctx.ArgString("limit")
	count := 0
	if len(key) > 0 {
		rs, code := ctx.NewIteratorWithField("key", key, limit)
		if code != SUCCESS {
			ctx.Log("test_kv_iterator NewIterator error")
			ctx.ErrorResult("test_kv_iterator NewIterator error")
			return
		}
		for rs.HasNext() {
			ec, code := rs.NextRow()
			if code != SUCCESS {
				ctx.Log("test_kv_iterator NewIterator NextRow error")
				ctx.ErrorResult("test_kv_iterator NewIterator NextRow error")
				return
			}
			k, _ := ec.GetString("key")
			v, _ := ec.GetBytes("value")
			ctx.Log("iterator k=" + k + " v=" + string(v))
			count++
		}
		rs.Close()
		ctx.SuccessResult(convert.Int32ToString(int32(count)))
	}
	{
		key = "aabcd1234"
		limit = "aabcd1248"
		rs, code := ctx.NewIteratorWithField("key", key, limit)
		if code != SUCCESS {
			ctx.Log("test_kv_iterator NewIterator error")
			ctx.ErrorResult("test_kv_iterator NewIterator error")
			return
		}
		for rs.HasNext() {
			ec, code := rs.NextRow()
			if code != SUCCESS {
				ctx.Log("test_kv_iterator NewIterator NextRow error")
				ctx.ErrorResult("test_kv_iterator NewIterator NextRow error")
				return
			}
			k, _ := ec.GetString("key")
			v, _ := ec.GetBytes("value")
			ctx.Log("iterator k=" + k + " v=" + string(v))
			count++
		}
		rs.Close()
	}
	{
		key = "key1234"
		limit = "key1236"
		rs, code := ctx.NewIterator(key, limit)
		if code != SUCCESS {
			ctx.Log("test_kv_iterator NewIterator error")
			ctx.ErrorResult("test_kv_iterator NewIterator error")
			return
		}
		for rs.HasNext() {
			ec, code := rs.NextRow()
			if code != SUCCESS {
				ctx.Log("test_kv_iterator NewIterator NextRow error")
				ctx.ErrorResult("test_kv_iterator NewIterator NextRow error")
				return
			}
			k, _ := ec.GetString("key")
			v, _ := ec.GetBytes("value")
			ctx.Log("iterator k=" + k + " v=" + string(v))
			count++
		}
		rs.Close()
	}
	{
		key = "aabcd1241"
		limit = "aabcd1271"
		rs, code := ctx.NewIteratorWithField("key", key, limit)
		if code != SUCCESS {
			ctx.Log("test_kv_iterator NewIterator error")
			ctx.ErrorResult("test_kv_iterator NewIterator error")
			return
		}
		for rs.HasNext() {
			ec, code := rs.NextRow()
			if code != SUCCESS {
				ctx.Log("test_kv_iterator NewIterator NextRow error")
				ctx.ErrorResult("test_kv_iterator NewIterator NextRow error")
				return
			}
			k, _ := ec.GetString("key")
			v, _ := ec.GetBytes("value")
			ctx.Log("iterator k=" + k + " v=" + string(v))
			count++
		}
		rs.Close()
	}
	{
		key = "aabcd1241"
		limit = "aabcf1261"
		rs, code := ctx.NewIteratorWithField("key", key, limit)
		if code != SUCCESS {
			ctx.Log("test_kv_iterator NewIterator error")
			ctx.ErrorResult("test_kv_iterator NewIterator error")
			return
		}
		for rs.HasNext() {
			ec, code := rs.NextRow()
			if code != SUCCESS {
				ctx.Log("test_kv_iterator NewIterator NextRow error")
				ctx.ErrorResult("test_kv_iterator NewIterator NextRow error")
				return
			}
			k, _ := ec.GetString("key")
			v, _ := ec.GetBytes("value")
			ctx.Log("iterator k=" + k + " v=" + string(v))
			count++
		}
		rs.Close()
	}
	ctx.SuccessResult(convert.Int32ToString(int32(count)))
}

//export test_put_state
func testPutState() {
	ctx := NewSimContext()
	key, _ := ctx.ArgString("key")
	value, _ := ctx.ArgString("value")
	if len(key) > 0 {
		ctx.PutState("key", key, value)
		ctx.Log("test_put_state finish key=" + key + "value=" + value)
		ctx.SuccessResult("ok")
		return
	}
	ctx.PutState("key", "aabcd1234", "aabcd1234")
	ctx.PutState("key", "aabcd1235", "aabcd1235")
	ctx.PutState("key", "aabcd1236", "aabcd1236")
	ctx.PutState("key", "aabcd1237", "aabcd1237")
	ctx.PutState("key", "aabcd1238", "aabcd1238")

	ctx.PutState("key", "aabcd1241", "aabcd1241")
	ctx.PutState("key", "aabcd1251", "aabcd1251")
	ctx.PutState("key", "aabcd1261", "aabcd1261")

	ctx.PutState("key", "aabcd1241", "aabcd1241")
	ctx.PutState("key", "aabce1251", "aabce1251")
	ctx.PutState("key", "aabcf1261", "aabcf1261")

	ctx.PutStateFromKey("key1234", "key1234")
	ctx.PutStateFromKey("key1235", "key1235")
	ctx.PutStateFromKey("key1236", "key1236")

	ctx.SuccessResult("ok")
}

//export test_put_pre_state
func testPutPreState() {
	ctx := NewSimContext()
	ctx.Log("============test_put_pre_state start============")
	key, _ := ctx.ArgString("key")
	field, _ := ctx.ArgString("field")
	value, _ := ctx.ArgString("value")
	if len(key) > 0 {
		ctx.PutState(key, field, value)
		ctx.Log("test_put_state finish key=" + key + " field" + field + "value=" + value)
		ctx.SuccessResult("ok")
		return
	}
	ctx.PutState("123", "pre1", "123")
	ctx.PutState("123", "pre11", "123")
	ctx.PutState("123", "pre9", "123")
	ctx.PutState("123", "pre91", "123")
	ctx.PutState("123", "prea", "123")
	ctx.PutState("123", "prea1", "123")
	ctx.PutState("123", "prez", "123")
	ctx.PutState("123", "prez1", "123")
	ctx.PutState("123", "pre.", "123")
	ctx.PutState("123", "pre.1", "123")
	ctx.PutState("123", "pre-", "123")
	ctx.PutState("123", "pre-1", "123")
	ctx.PutState("123", "pre_", "123")
	ctx.PutState("123", "pre_1", "123")

	ctx.PutStateFromKey("pre1", "123")
	ctx.PutStateFromKey("pre11", "123")
	ctx.PutStateFromKey("pre9", "123")
	ctx.PutStateFromKey("pre91", "123")
	ctx.PutStateFromKey("prea", "123")
	ctx.PutStateFromKey("prea1", "123")
	ctx.PutStateFromKey("prez", "123")
	ctx.PutStateFromKey("prez1", "123")
	ctx.PutStateFromKey("pre.", "123")
	ctx.PutStateFromKey("pre.1", "123")
	ctx.PutStateFromKey("pre-", "123")
	ctx.PutStateFromKey("pre-1", "123")
	ctx.PutStateFromKey("pre_", "123")
	ctx.PutStateFromKey("pre_1", "123")
	ctx.Log("============test_put_pre_state end============")
}

//export test_iter_pre_field
func testIter() {
	ctx := NewSimContext()
	ctx.Log("============test_iter start============")

	count := 0

	rs, code := ctx.NewIteratorPrefixWithKeyField("123", "pre1")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKeyField("123", "pre9")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKeyField("123", "prea")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKeyField("123", "prez")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKeyField("123", "pre.")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKeyField("123", "pre-")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKeyField("123", "pre_")
	count += parseResultSet(ctx, rs, code)

	ctx.SuccessResult(convert.Int32ToString(int32(count)))
	ctx.Log("============test_iter end============")
}

//export test_iter_pre_key
func testIterPre() {
	ctx := NewSimContext()
	ctx.Log("============test_iter start============")

	count := 0

	rs, code := ctx.NewIteratorPrefixWithKey("pre1")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKey("pre9")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKey("prea")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKey("prez")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKey("pre.")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKey("pre_")
	count += parseResultSet(ctx, rs, code)
	rs, code = ctx.NewIteratorPrefixWithKey("pre-")
	count += parseResultSet(ctx, rs, code)

	ctx.SuccessResult(convert.Int32ToString(int32(count)))
	ctx.Log("============test_iter end============")
}

func parseResultSet(ctx SimContext, rs ResultSetKV, code ResultCode) int {
	count := 0
	if code != SUCCESS {
		ctx.Log("test_iter NewIteratorPrefixWithKey error")
		ctx.ErrorResult("test_iter NewIteratorPrefixWithKey error")
		panic("err")
		//return 0, errors.New("err")
	}
	for rs.HasNext() {
		key, field, value, code := rs.Next()
		if code != SUCCESS {
			ctx.Log("test_iter NewIteratorPrefixWithKey NextRow error")
			ctx.ErrorResult("test_iter NewIteratorPrefixWithKey NextRow error")
			panic("err")
			//return count, errors.New("err")
		}
		ctx.Log("iterator key=" + key + " field=" + field + " value=" + string(value))
		count++
	}
	rs.Close()
	return count
}
func main() {

}
