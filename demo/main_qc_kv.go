package main

// feat 测试用例
import (
	"chainmaker.org/contract-sdk-tinygo/sdk/convert"
	"encoding/base64"
	"strings"
)

// 安装合约时会执行此方法，必须
//export init_contract
func initContract() {
	LogMessage("[zitao] init test")
}

//export increase
func increase() {
	LogMessage("call increase")
	var heartbeatInt int32 = 0
	if heartbeatString, resultCode := GetState("Counter", "heartbeat"); resultCode != SUCCESS {
		heartbeatInt = 1
		LogMessage("call increase, put state from empty")
		PutState("Counter", "heartbeat", convert.Int32ToString(heartbeatInt))
	} else {
		heartbeatInt, _ = convert.StringToInt32(heartbeatString)
		heartbeatInt++
		LogMessage("call increase, put state from exist")
		PutState("Counter", "heartbeat", convert.Int32ToString(heartbeatInt))
	}
	SuccessResult(convert.Int32ToString(heartbeatInt))
}

//export query
func query() {
	LogMessage("call query")
	if heartbeatString, resultCode := GetState("Counter", "heartbeat"); resultCode != SUCCESS {
		LogMessage("failed to query state")
		SuccessResult("0")
	} else {
		LogMessage("call query, heartbeat:" + heartbeatString)
		SuccessResult(heartbeatString)
	}
}

type person struct {
	Name string `json:"name"`
	Age  int64  `json:"age"`
}

//export for_dag
func for_dag() {
	if txId, resultCode := GetTxId(); resultCode != SUCCESS {
		ErrorResult("failed to get tx id")
	} else {
		for i := 0; i < 2; i++ {
			PutState("test", txId[i:i+1], "value")
		}
		SuccessResult("ok")
	}
}

//
////export json_ex
//func json_ex() {
//	if num, resultCode := ArgString("num"); resultCode != SUCCESS {
//		ErrorResult("failed to get num")
//		return
//	} else {
//		LogMessage("num " + num)
//	}
//	para_str, _ := ArgString("str")
//	LogMessage("m[\"str\"] = " + para_str)
////	LogMessage("m[\"str\"] = " + m["str"].(string))
//	var sth map[string]interface{}
//	para_sth, _ := ArgString("sth")
//	//sth = para_sth  //.(map[string]interface{})
//	json.Unmarshal([]byte(para_sth), &sth)
//	LogMessage("m[\"sth\"][\"num1\"] = " + strconv.FormatInt(sth["num1"].(int64), 10))
//	LogMessage("m[\"sth\"][\"num2\"] = " + strconv.FormatInt(sth["num2"].(int64), 10))
//	LogMessage("m[\"sth\"][\"str1\"] = " + sth["str1"].(string))
//	LogMessage("m[\"sth\"][\"str2\"] = " + sth["str2"].(string))
//	var persons []interface{}
//	persons = sth["persons"].([]interface{})
//	var person1 = persons[0].(map[string]interface{})
//	var person2 = persons[1].(map[string]interface{})
//	var p1, p2 person
//	p1.Name = person1["name"].(string)
//	p1.Age = person1["age"].(int64)
//	p2.Name = person2["name"].(string)
//	p2.Age = person2["age"].(int64)
//	LogMessage("person1: " + p1.Name + " age " + strconv.FormatInt(p1.Age, 10))
//	LogMessage("person2: " + p2.Name + " age " + strconv.FormatInt(p2.Age, 10))
//	// _, err := strconv.ParseInt("-1", 10, 64)
//	// if err != nil {
//	// 	LogMessage(err.Error())
//	// }
//	// strconv.FormatInt(1e9, 10)
//}

//export calc_json_int64
func calc_json_int64() {
	LogMessage("[zitao] input func: calc_json1")
	//calc_json := Args()
	//func_name := calc_json["func_name"].Value.(string)
	//data1 := calc_json["data1"].Value.(string)
	//data2 := calc_json["data2"].Value.(string)
	//data3 := calc_json["data3"].Value.(string)
	func_name, _ := ArgString("func_name")
	data1, _ := ArgString("data1")
	data2, _ := ArgString("data2")
	data3, _ := ArgString("data3")
	LogMessage("[zitao] calc_json[func_name]: " + func_name)
	LogMessage("[zitao] calc_json[data1]: " + data1)
	LogMessage("[zitao] calc_json[data2]: " + data2)
	LogMessage("[zitao] calc_json[data3]: " + data3)

	idata1, _ := convert.StringToInt64(data1)
	idata2, _ := convert.StringToInt64(data2)
	var result_str string
	var result int64
	status := false
	if func_name == "add" {
		result = idata1 + idata2
		result_str = convert.Int64ToString(result)
		status = true
	} else if func_name == "sub" {
		result = idata1 - idata2
		result_str = convert.Int64ToString(result)
		status = true
	} else if func_name == "mul" {
		result = idata1 * idata2
		result_str = convert.Int64ToString(result)
		status = true
	} else if func_name == "div" {
		result = idata1 / idata2
		result_str = convert.Int64ToString(result)
		status = true
	} else if func_name == "set_data" {
		result_str = data3
		status = true
	} else if func_name == "failure" {
		LogMessage("[zitao] calc_json[func_name] failure result: " + func_name)
		ErrorResult("zitao test error")
	} else if func_name == "delete" {
		LogMessage("[zitao] calc_json[func_name] DeleteState result: " + func_name)
		DeleteState("zitao", func_name)
	} else {
		LogMessage("[zitao] panic test")
		panic("zitao test panic !!!")
	}
	if status {
		PutState("zitao", func_name, result_str)
		LogMessage("[zitao] calc_json[func_name] result: " + result_str)
	}
	SuccessResult("ok")
}

//export calc_json
func calc_json() {
	LogMessage("[zitao] input func: calc_json")
	//calc_json := Args()
	func_name, _ := ArgString("func_name")
	data1, _ := ArgString("data1")
	data2, _ := ArgString("data2")
	LogMessage("[zitao] calc_json[func_name]: " + func_name)
	LogMessage("[zitao] calc_json[data1]: " + data1)
	LogMessage("[zitao] calc_json[data2]: " + data2)

	idata1, _ := convert.StringToInt32(data1)
	idata2, _ := convert.StringToInt32(data2)
	LogMessage("[zitao] change calc_json[data1]: " + convert.Int32ToString(idata1))
	LogMessage("[zitao] change calc_json[data2]: " + convert.Int32ToString(idata2))
	var result_str string
	var result int32
	status := false
	if func_name == "add" {
		result = idata1 + idata2
		result_str = convert.Int32ToString(result)
		status = true
	} else if func_name == "sub" {
		result = idata1 - idata2
		result_str = convert.Int32ToString(result)
		status = true
	} else if func_name == "mul" {
		result = idata1 * idata2
		result_str = convert.Int32ToString(result)
		status = true
	} else if func_name == "div" {
		result = idata1 / idata2
		result_str = convert.Int32ToString(result)
		status = true
	} else if func_name == "set_data" {
		data3, _ := ArgString("data3")
		data4, _ := ArgString("data4")
		LogMessage("[zitao] calc_json[data3]: " + data3)
		LogMessage("[zitao] calc_json[data4]: " + data4)
		PutState("zitao", data3, data4)
		LogMessage("[zitao] calc_json[func_name] result: " + result_str)
		status = true
	} else if func_name == "failure" {
		data3, _ := ArgString("data3")
		LogMessage("[zitao] calc_json[data3]: " + data3)
		LogMessage("[zitao] calc_json[func_name] failure set result: " + data3)
		PutState("zitao", func_name, data3)
		ErrorResult("zitao test error")
	} else if func_name == "failure_succes" {
		data3, _ := ArgString("data3")
		LogMessage("[zitao] calc_json[data3]: " + data3)
		LogMessage("[zitao] calc_json[func_name] failure set result: " + data3)
		PutState("zitao", func_name, data3)
		ErrorResult("zitao test error - failure_succes")
		SuccessResult("ok")
	} else if func_name == "delete" {
		data3, _ := ArgString("data3")
		LogMessage("[zitao] calc_json[data3]: " + data3)
		LogMessage("[zitao] calc_json[func_name] delete name: " + data3)
		DeleteState("zitao", data3)
		status = true
	} else {
		LogMessage("[zitao] panic test")
		PutState("zitao", func_name, "panic")
		panic("zitao test panic !!!")
	}
	if status {
		PutState("zitao", func_name, result_str)
		LogMessage("[zitao] calc_json[func_name] result: " + result_str)
		SuccessResult("ok")
	}
}

//export get_calc
func get_calc() {
	LogMessage("[zitao] input func: get_json")
	func_name, resultCode := ArgString("func_name")
	if resultCode != SUCCESS {
		ErrorResult("failure get func_name")
		return
	}
	LogMessage("[zitao] get_calc[func_name]: " + func_name)

	result, _ := GetState("zitao", func_name)
	LogMessage("[zitao] calc_json[func_name] result: " + result)
	SuccessResult(result)
}

//export call_self
func call_self() {
	LogMessage("[zitao] input func: call_self")

	callnum_str, _ := GetState("zitao", "callnum")
	icallnum, _ := convert.StringToInt32(callnum_str)
	LogMessage("[zitao] change calc_json[callnum]: " + convert.Int32ToString(icallnum))
	icallnum = icallnum - 1
	PutState("zitao", "callnum", convert.Int32ToString(icallnum))
	if icallnum < 1 {
		LogMessage("[zitao] call_self[callnum] result(end): " + convert.Int32ToString(icallnum))
		SuccessResult("finish call_self")
	} else {
		LogMessage("[zitao] call_self[callnum] result(test): " + convert.Int32ToString(icallnum))
		call_self()
	}
}

//export loop_test
func loop_test() {
	LogMessage("[zitao] input func: loop_test")

	loopnum_str, _ := GetState("zitao", "loopnum")
	iloopnum, _ := convert.StringToInt32(loopnum_str)
	LogMessage("[zitao] change loop_test[loopnum]: " + convert.Int32ToString(iloopnum))
	for i := iloopnum; i > 1; i-- {
		LogMessage("[zitao] change loop_test[i]: " + convert.Int32ToString(i))
		PutState("zitao", "loopnum", convert.Int32ToString(i))
	}

	SuccessResult("finish loop_test")
}

//export set_store
func set_store() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: set_store")

	key, _ := ArgString("key")
	name, _ := ArgString("name")
	value, _ := ArgString("value")
	LogMessage("[zitao] change set_store[key]: " + key)
	LogMessage("[zitao] change set_store[name]: " + name)
	LogMessage("[zitao] change set_store[value]: " + value)
	result := PutState(key, name, value)
	LogMessage("[zitao] PutState: key=" + key + ",name=" + name + ",value=" + value + ",result:" + convert.Int32ToString(int32(result)))
	LogMessage("[zitao] ========================================end")
	SuccessResult("finish set_store")
}

//export get_store
func get_store() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: get_store")
	key, _ := ArgString("key")
	name, _ := ArgString("name")
	LogMessage("[zitao] change get_store[key]: " + key)
	LogMessage("[zitao] change get_store[name]: " + name)
	value, result := GetState(key, name)
	LogMessage("[zitao] GetState: key=" + key + ",name=" + name + ",result:" + convert.Int32ToString(int32(result)))
	LogMessage("[zitao] change get_store[value]: " + value)
	LogMessage("[zitao] ========================================end")
	SuccessResult(value)
}

//export delete_store
func delete_store() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: delete_store")
	key, _ := ArgString("key")
	name, _ := ArgString("name")
	LogMessage("[zitao] change delete_store[key]: " + key)
	LogMessage("[zitao] change delete_store[name]: " + name)
	result := DeleteState(key, name)
	LogMessage("[zitao] DeleteState: key=" + key + ",name=" + name + ",result:" + convert.Int32ToString(int32(result)))
	LogMessage("[zitao] ========================================end")
	SuccessResult("finish delete_store")
}

//export set_store_no_log
func set_store_no_log() {
	key, _ := ArgString("key")
	name, _ := ArgString("name")
	value, _ := ArgString("value")
	PutState(key, name, value)
	SuccessResult("finish set_store")
}

//export get_store_no_log
func get_store_no_log() {
	key, _ := ArgString("key")
	name, _ := ArgString("name")
	value, _ := GetState(key, name)
	SuccessResult(value)
}

//export event_test_set
func event_test_set() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: event_test_set")

	key, _ := ArgString("key")
	name, _ := ArgString("name")
	value, _ := ArgString("value")
	LogMessage("[zitao] change event_test_set[key]: " + key)
	LogMessage("[zitao] change event_test_set[name]: " + name)
	LogMessage("[zitao] change event_test_set[value]: " + value)
	result := PutState(key, name, value)
	LogMessage("[zitao] PutState: key=" + key + ",name=" + name + ",value=" + value + ",result:" + convert.Int32ToString(int32(result)))
	value_list := strings.Split(value, "#")
	EmitEvent(key, value_list...)
	LogMessage("[zitao] ========================================end")
	SuccessResult("finish event_test_set")
}

//export event_test_get
func event_test_get() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: event_test_get")
	key, _ := ArgString("key")
	name, _ := ArgString("name")
	LogMessage("[zitao] change event_test_get[key]: " + key)
	LogMessage("[zitao] change event_test_get[name]: " + name)
	value, result := GetState(key, name)
	LogMessage("[zitao] GetState: key=" + key + ",name=" + name + ",result:" + convert.Int32ToString(int32(result)))
	value_list := strings.Split(name, "#")
	EmitEvent(key, value_list...)
	LogMessage("[zitao] change event_test_get[value]: " + value)
	LogMessage("[zitao] ========================================end")
	SuccessResult(value)
}

//export event_test_set_no_log
func event_test_set_no_log() {
	key, _ := ArgString("key")
	name, _ := ArgString("name")
	value, _ := ArgString("value")
	PutState(key, name, value)
	value_list := strings.Split(value, "#")
	EmitEvent(key, value_list...)
	SuccessResult("finish event_test_set")
}

//export event_test_get_no_log
func event_test_get_no_log() {
	key, _ := ArgString("key")
	name, _ := ArgString("name")
	value, _ := GetState(key, name)
	value_list := strings.Split(name, "#")
	EmitEvent(key, value_list...)
	SuccessResult(value)
}

//export paillier_test_set
func paillier_test_set() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: paillier_test_set")

	pubkeyBytes, _ := ArgString("pubkey")
	handletype, _ := ArgString("handletype")
	encodePara1, _ := ArgString("para1")
	encodePara2, _ := ArgString("para2")
	LogMessage("[zitao] change paillier_test_set[pubkeyBytes]: " + pubkeyBytes)
	LogMessage("[zitao] change paillier_test_set[handletype]: " + handletype)
	LogMessage("[zitao] change paillier_test_set[para1]: " + encodePara1)
	LogMessage("[zitao] change paillier_test_set[para2]: " + encodePara2)

	para1Bytes, _ := base64.StdEncoding.DecodeString(encodePara1)
	//para1 := string(para1Bytes)
	//para2 := string(para2Bytes)
	var result_code ResultCode
	var result_data []byte
	var result_data_str string
	test := NewPaillierContext()
	if handletype == "AddCiphertext" {
		para2Bytes, _ := base64.StdEncoding.DecodeString(encodePara2)
		result_data, result_code = test.AddCiphertext([]byte(pubkeyBytes), para1Bytes, para2Bytes)
	} else if handletype == "AddPlaintext" {
		result_data, result_code = test.AddPlaintext([]byte(pubkeyBytes), para1Bytes, encodePara2)
	} else if handletype == "SubCiphertext" {
		para2Bytes, _ := base64.StdEncoding.DecodeString(encodePara2)
		result_data, result_code = test.SubCiphertext([]byte(pubkeyBytes), para1Bytes, para2Bytes)
	} else if handletype == "SubPlaintext" {
		result_data, result_code = test.SubPlaintext([]byte(pubkeyBytes), para1Bytes, encodePara2)
	} else if handletype == "NumMul" {
		result_data, result_code = test.NumMul([]byte(pubkeyBytes), para1Bytes, encodePara2)
	} else {
		ErrorResult("finish event_test_set failure: error para: " + handletype)
	}
	if result_code != SUCCESS {
		ErrorResult("finish event_test_set failure: error result code: " + string(result_code))
	}

	result_data_str = base64.StdEncoding.EncodeToString(result_data)

	result := PutState("paillier_test", handletype, result_data_str)
	LogMessage("[zitao] PutState: key=paillier_test_set" + ",name=" + handletype + ",value=" + result_data_str + ",result:" + convert.Int32ToString(int32(result)))
	LogMessage("[zitao] ========================================end")
	if result_code == 0 {
		SuccessResult("finish event_test_set success")
	} else {
		ErrorResult("finish event_test_set failure")
	}
}

//export paillier_test_get
func paillier_test_get() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: paillier_test_get")
	handletype, _ := ArgString("handletype")
	value, result := GetState("paillier_test", handletype)
	LogMessage("[zitao] GetState: key=key=paillier_test_get" + ",name=" + handletype + ",value=" + value + ",result:" + convert.Int32ToString(int32(result)))
	LogMessage("[zitao] change event_test_get[value]: " + value)
	LogMessage("[zitao] ========================================end")
	SuccessResult(value)
}

//export bulletproofs_test_set
func bulletproofs_test_set() {
	LogMessage("[bulletproofs] ========================================start")
	LogMessage("[bulletproofs] bulletproofs_test_set")
	handleType, _ := ArgString("handletype")
	param1, _ := ArgString("para1")
	param2, _ := ArgString("para2")

	param1Bytes, _ := base64.StdEncoding.DecodeString(param1)
	//param2Bytes, _ := base64.StdEncoding.DecodeString(param2)
	var result_code ResultCode
	var result_data []byte
	var result_data_str string
	bulletproofsContext := NewBulletproofsContext()
	switch handleType {
	case BulletproofsOpTypePedersenAddNum:
		result_data, result_code = bulletproofsContext.PedersenAddNum(param1Bytes, param2)
	case BulletproofsOpTypePedersenAddCommitment:
		param2Bytes, _ := base64.StdEncoding.DecodeString(param2)
		result_data, result_code = bulletproofsContext.PedersenAddCommitment(param1Bytes, param2Bytes)
	case BulletproofsOpTypePedersenSubNum:
		result_data, result_code = bulletproofsContext.PedersenSubNum(param1Bytes, param2)
	case BulletproofsOpTypePedersenSubCommitment:
		param2Bytes, _ := base64.StdEncoding.DecodeString(param2)
		result_data, result_code = bulletproofsContext.PedersenSubCommitment(param1Bytes, param2Bytes)
	case BulletproofsOpTypePedersenMulNum:
		result_data, result_code = bulletproofsContext.PedersenMulNum(param1Bytes, param2)
	case BulletproofsVerify:
		param2Bytes, _ := base64.StdEncoding.DecodeString(param2)
		result_data, result_code = bulletproofsContext.Verify(param1Bytes, param2Bytes)
	default:
		ErrorResult("bulletproofs_test_set failed, error: " + handleType)
		result_code = 1
	}

	if result_code != SUCCESS {
		ErrorResult("bulletproofs_test_set failed, error: " + string(rune(result_code)))
	}

	result_data_str = base64.StdEncoding.EncodeToString(result_data)

	result := PutState("bulletproofs_test", handleType, result_data_str)
	LogMessage("[bulletproofs] PutState: key=bulletproofs_test" + ",name=" + handleType + ",value=" + result_data_str + ",result:" + convert.Int32ToString(int32(result)))
	LogMessage("[bulletproofs] ========================================end")
	if result_code == 0 {
		SuccessResult("bulletproofs_test_set success")
	} else {
		ErrorResult("bulletproofs_test_set failure")
	}
}

//export bulletproofs_test_get
func bulletproofs_test_get() {
	LogMessage("[bulletproofs] ========================================start")
	LogMessage("[bulletproofs] input func: paillier_test_get")
	handletype, _ := ArgString("handletype")
	value, result := GetState("bulletproofs_test", handletype)
	LogMessage("[bulletproofs] GetState: key=key=bulletproofs_test" + ",name=" + handletype + ",value=" + value + ",result:" + convert.Int32ToString(int32(result)))
	LogMessage("[bulletproofs] change event_test_get[value]: " + value)
	LogMessage("[bulletproofs] ========================================end")
	if handletype == "BulletproofsVerify" {
		decodeValue, err := base64.StdEncoding.DecodeString(value)
		if err != nil {
			ErrorResult("base64.StdEncoding.DecodeString(value) failed")
		}
		SuccessResult(string(decodeValue))
	} else {
		SuccessResult(value)
	}
}

func main() {

}
