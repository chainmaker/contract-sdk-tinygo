/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0

main_sql_perf.go
*/
package main

import (
	"chainmaker.org/contract-sdk-tinygo/sdk/convert"
)

//安装合约时会执行此方法，必须
//export init_contract
func initContract() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")

	// create teacher
	sqlCreateTeacher := `create table performance_go (
							id varchar(128) primary key,
							number int
						)
						`
	ctx.Log(sqlCreateTeacher)
	_, resultCode := ctx.ExecuteDdl(sqlCreateTeacher)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlCreateTeacher=" + sqlCreateTeacher
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("create table performance_go success.")
	}

	ctx.SuccessResult("create table student、teacher_gasm success")
	ctx.Log("initContract success.")
	ctx.Log("init_contract [end]")
}

// 升级合约时会执行此方法，必须
//export upgrade
func upgrade() {
	ctx := NewSqlSimContext()
	ctx.Log("upgrade [start]")
	ctx.Log("upgrade [end]")
}

//export sql_insert
func sqlInsert() {
	ctx := NewSqlSimContext()

	id, _ := ctx.ArgString("id")
	number, _ := ctx.ArgString("number")

	if len(id) == 0 || len(number) == 0 {
		ctx.Log("param id/age is required")
		ctx.ErrorResult("param id/age is required")
		return
	}

	sqlInsert := "insert into performance_go(id, number) VALUES ('" + id + "', " + number + ")"

	_, resultCode := ctx.ExecuteUpdate(sqlInsert)
	if resultCode != SUCCESS {
		ctx.Log("sql_insert error")
		ctx.ErrorResult("sql_insert error")
		return
	}
}

//export sql_update
func sqlUpdate() {
	ctx := NewSqlSimContext()

	id, _ := ctx.ArgString("id")
	number, _ := ctx.ArgString("number")

	sqlInsert := "update  performance_go set number=" + number + " where id='" + id + "' "

	_, resultCode := ctx.ExecuteUpdate(sqlInsert)

	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("ExecuteUpdateSingle error")
		return
	}
}

//export sql_query_number
func sqlQueryNumber() {
	ctx := NewSqlSimContext()

	sql := "select max(number) num from performance_go"

	ec, resultCode := ctx.ExecuteQueryOne(sql)

	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("-1")
		return
	}
	num, _ := ec.GetString("num")
	ctx.SuccessResult(num)
}

//export sql_query_count
func sqlQueryCount() {
	ctx := NewSqlSimContext()

	sql := "select count(*) num from performance_go"

	ec, resultCode := ctx.ExecuteQueryOne(sql)

	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("-1")
		return
	}
	num, _ := ec.GetString("num")
	ctx.SuccessResult(num)
}

//export sql_blank
func sqlBlank() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_blank [start]")
	ctx.Log("sql_blank [end]")
}

func main() {

}
